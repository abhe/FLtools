
import torch
from config import get_objects
import flutils
import mlflow
import random
import time

cuda = torch.device('cuda')
cpu = torch.device('cpu')
from mltools import torchutils
NUMBER_ROUNDS = 100


# GET MODEL
# model = torchutils.load_model(YNet3, checkpoint_load, overwrite=checkpoint_overwrite, in_channels=in_channels, f_maps=f_maps, grade_mapping=GleasonGrade_mapping, learning_objective=learning_objective, learning_rate=learning_rate, learning_opt=learning_optimizer)


class FlareSimulation:
    """
    Simulates federated learning loop without the need for NVFlare.

    Methods
    --------
    performs_normal_training()
        performs normal training

    prepare_fl_models()
        Initializes the server and the site models

    fl_fit()
        Performs the federated training

    """
    def __init__(self):
        """
        Prepares the data and initlizes the model for normal training
        """
        mlflow.set_tracking_uri("../mlruns")
        self.experiment_id = mlflow.create_experiment(f"Experiment{random.randint(100,9000)}")
        self.experiment = mlflow.get_experiment(self.experiment_id)
        self.run = mlflow.active_run()
        if self.run is  None:
            mlflow.start_run()
            self.run = mlflow.active_run()
        print("run_id: {}; status: {}".format(self.run.info.run_id, self.run.info.status))
        mlflow.log_param("NUMBER_ROUNDS", NUMBER_ROUNDS)

    def perform_normal_training(self):
        """
        Performs the normal training

        Parameters
        ----------
        None

        """
        pass

    def prepare_fl_models(self):
        """
        Initializes the server and the site models

        Parameters
        ----------
        None

        Returns
        ----------
        None
        
        """
        self.clients_list = ['ucsf','ucla']
        self.client_devices = {'ucsf':cuda,'ucla':cuda}
        self.server_objects = get_objects()
        self.server_model = self.server_objects['model'].to(cuda)
        self.client_objects = {}
        for c in self.clients_list:
            self.client_objects[c] = get_objects()
            self.client_objects[c]['enumerator'] = enumerate(self.client_objects[c]['dataloader_train'])
            self.client_objects[c]['model'].to(cuda)

    def print_state_dict_details(self,model, model_name):
        print(f"\nParamerter Size Details for model {model_name}\n"
              f"------------------------------------\n")
        for n, p in model.named_parameters():
            print(f"parameter name is {n} \n"
                  f"Parameter size is {p.size() if p is not None else None} \n"
                  f"Required gradient is {p.requires_grad}\n"
                  f"Parameter data size is {p.data.size() if p.data is not None else None}\n"
                  f"Parameter gradient size is {p.grad.size() if p.grad is not None else None}"
                f"\n"
            )

    def print_ordered_dict_details(self, dict, dict_name):
        print(f"\nOrdered Dict Size Details for dict {dict_name} \n"
              f"--------------------------------------\n")
        for k,v in dict.items():
            print(f"The key is {k} and the value size is {v.shape} ")

    def print_loss(self, client, rnd, loss):
            print ('Round {}, Loss: {:.4f} for client {}'
                .format(rnd, loss.item(), client))

    def fl_fit(self):
        """
        Performs the federated training loop

        Parameters
        ----------
        None

        """
        print("Fitting federated loop")
        global_time_start = time.time()
        # Server and client models are already initialized. 
        # Setting the optimizer.
        opt = self.server_model.configure_optimizers()
        self.print_state_dict_details(self.server_model, "Server Model")

        # map to collect the gradients in a paticular round. 
        client_gradient_map = {}
        for r in range(NUMBER_ROUNDS):
            round_time_start = time.time()
            print(f"Starting round {r}")
            # Getting the weights from the server
            sw = flutils.get_weights_order_dict(self.server_model,cpu)
            print("Using Server model get weight order dict")
            #In each round train on every client.
            
            for c in self.clients_list:

                flutils.replace_model_weights_from_ordered_dict(self.client_objects[c]['model'],sw)
                print("Replaced client weights with server weight order dict")
                # Getting the iterator for the client
                try:
                    # Getting the batch using the iterator
                    print("getting a batch using client train dataloder")
                    batch_start = time.time()
                    batch_idx, batch = next(self.client_objects[c]['enumerator'])
                    batch_end = time.time()
                    print(f"finished getting a batch;time taken {round(batch_end - batch_start)} seconds.")
                except StopIteration:
                    self.client_objects[c]['enumerator'] = enumerate(self.client_objects[c]['dataloader_train'])
                    batch_idx, batch = next(self.client_objects[c]['enumerator'])

                #Doing forward and backward on a client
                loss = flutils.do_site_iteration(self.client_objects[c]['model'],batch, batch_idx)
                #Getting the gradients 
                local_model_dict, local_graident_dict = flutils.get_weights_gradients_order_dict(self.client_objects[c]['model'],cpu)
                # placing it in the map
                client_gradient_map[c] = local_graident_dict
                #self.print_ordered_dict_details(local_graident_dict, f"{c} Dict")
                self.print_loss(c, r, loss)
                #self.client_objects[c].log("Loss/"+c, loss, r)
                mlflow.log_metric(f"loss_{c}", float("{:.4f}".format(loss)))

            # Aggreating and placing the aggregated weights on to the server
            flutils.aggregate_gradients(self.server_model,client_gradient_map)
            # Running optmizer setp on the server
            opt.step()
            print(f"Completed round {r} \n")
            round_time_end = time.time()
            print(f"Total time to complete the round {r} is {round(round_time_end - round_time_start)} seconds.")
            # Completed this round ready from next round.
        global_time_end = time.time()
        print(f"Total time to complete the training is {round(global_time_end - global_time_start)} seconds.")

if __name__ == "__main__":
    fls = FlareSimulation()
    print(fls.__doc__)
    fls.perform_normal_training()
    fls.prepare_fl_models()
    fls.fl_fit()
