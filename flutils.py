import torch
from pytorch_lightning.metrics.functional import accuracy


#batch and batch_idx are provided Make forward pass and backward pass and return the loss. 
# Loss would be used for reporting. 
def do_site_iteration(model,batch,batch_idx):
    #batch = {k:v.to(model.device) for k,v in batch.items() if not 'file' in k}
    loss = model.training_step(batch, batch_idx)
    model.zero_grad()
    loss.backward()
    return loss

#Getting the orderdict from the state_dict both for the weights and gradients. 
#Sharable objects needs numpy arrays. 
# Abhe need to check if this is the correct way to get the gradients.
def get_weights_gradients_order_dict(model,device):
    local_state_dict = model.state_dict(keep_vars=True)
    order_weight_dict = {}
    order_gradient_dict = {}
    for var_name in local_state_dict.keys():
        if local_state_dict[var_name].requires_grad and local_state_dict[var_name].grad is not None:
            order_weight_dict[var_name] = local_state_dict[var_name].to(device).detach().numpy()
            order_gradient_dict[var_name] = local_state_dict[var_name].grad.to(device).numpy()
        else:
            pass
            #print("@@@@ var_name ", var_name, " does not have gradient")

    return order_weight_dict, order_gradient_dict

def get_weights_order_dict(model,device):
    local_state_dict = model.state_dict(keep_vars=True)
    order_weight_dict = {}
    for var_name in local_state_dict.keys():
        if local_state_dict[var_name].requires_grad :
            order_weight_dict[var_name] = local_state_dict[var_name].to(device).detach().numpy()
    return order_weight_dict

#This methods takes numpy weights from the server and replaces the weights in the model. 
def replace_model_weights_from_ordered_dict(model,model_weights):
    local_var_dict = model.state_dict()
    model_keys = model_weights.keys()
    for var_name in local_var_dict:
        if var_name in model_keys:
            weights = model_weights[var_name]
            local_var_dict[var_name] = torch.as_tensor(weights)

    # Creating the model by loading from the above state_dict. 
    model.load_state_dict(local_var_dict)

    # def configure_optimizer(self):
    #     return self.model.configure_optimizers()

#This methods is used in the aggregate method on the server. 
#This is the implementation of the fed_sgd
def aggregate_gradients(model, client_gradient_map ):
    gradient_dict = {}

    # For each variable we will loop though all the clients and collect the gradients in np_grap[]
    # We will then perform the algorithm on the gradients. 

    #Looping through each variable
    for v_name in client_gradient_map[list(client_gradient_map.keys())[0]].keys():
        np_grads = []
        # For each client collecting the gradients. 
        for client_name, gradients in client_gradient_map.items():
            if v_name not in gradients.keys():
                print("@@@@ Found a key not in data")
                pass
            else:
                np_grads.append(gradients[v_name])
        
        #print("Variable name is ", v_name)
        #print("Lenght of the np_grads ",len(np_grads))

        if len(np_grads) != 0:
            # Creating the resultant gradient tensor. 
            result_gradient = torch.zeros_like(torch.as_tensor(np_grads[0]))
            for grad in np_grads:
                result_gradient = result_gradient+grad
            result_gradient = result_gradient/len(np_grads)
            gradient_dict[v_name] = result_gradient
    
    # Taking the gradient and populating it into the model. 
    # The graidents are directly assigned to the .grad as mentioned in the pytorch documentation
    # https://pytorch.org/docs/stable/autograd.html#default-gradient-layouts
    local_dict = model.state_dict(keep_vars=True)
    #pdb.set_trace()
    for key, value in gradient_dict.items():
        local_dict[key].grad = gradient_dict[key].to(local_dict[key].device)
    #pdb.set_trace()
    # Loading the gradients back into the server model
    model.load_state_dict(local_dict) 


def log(self, name,value, round):
    self.writer.add_scalar(name, value, round)