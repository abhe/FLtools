from typing import Tuple
from nvflare.apis.aggregator import Aggregator
from nvflare.apis.fl_constant import FLConstants, ShareableKey, ShareableValue
from nvflare.apis.fl_context import FLContext
from nvflare.apis.shareable import Shareable
from nvflare.utils.fl_ctx_sanity_check import server_fl_ctx_sanity_check
from nvflare.apis.event_type import EventType
import importlib
import torch
from config import get_objects
import flutils
from mltools import torchutils

cuda = torch.device('cuda')
cpu = torch.device('cpu')



class FLAggregator(Aggregator):
    """ This class extends Aggregator class which has the capability to accumulate the weights and gradients 
    sent from the clients to the server and aggreagate them. 
    """
    def __init__(self):
        super().__init__()
        self.logger.info("@@@@Inside AccumulateWeightedAggregator in init")
        self.accumulator = []
        self.client_gradient_map = {}
        self.objects = get_objects()

    def handle_event(self, event_type: str, fl_ctx: FLContext):
        """In the START_RUN event the model is initalized and placed in the sharable object
        This model severs as the skeleton to receive the gradients from the clients. 
        This sharable object is placed in the FL_CONTEXT. Later this object is used Aggregate method.
        """
        if event_type == EventType.START_RUN:
            self.logger.info("@@@@ In Aggregator Start Run")

            # Initialize the Model.
            self.model = self.objects['model'].to(cuda)
            self.opt = self.model.configure_optimizers()

            # Creating the order_dict from the model. 
            local_state_dict = self.model.state_dict()
            local_model_dict = {}
            for var_name in local_state_dict:
                try:
                    local_model_dict[var_name] = local_state_dict[var_name].cpu().numpy()
                except Exception as e:
                    raise ValueError("Convert weight from {} failed with error: {}".format(var_name, str(e)))

            # Placing the order_dict and the actual model as well. 
            shareable = Shareable()
            shareable[ShareableKey.MODEL_WEIGHTS] = local_model_dict
            shareable[ShareableKey.TYPE] = ShareableValue.TYPE_WEIGHTS
            shareable[ShareableKey.DATA_TYPE] = ShareableValue.DATA_TYPE_UNENCRYPTED
            # Putting the sharable and model into the FL Context.
            fl_ctx.set_prop(FLConstants.SHAREABLE, shareable)
            fl_ctx.set_prop(FLConstants.MODEL_NETWORK, self.model)

        elif event_type == EventType.END_RUN:
            self.logger.info("@@@@ In Aggregate End Run")


    def accept(self, shareable: Shareable, fl_ctx: FLContext) -> Tuple[(bool, bool)]:
        """This method accumulates all the sharable fl_context from the clients.
            When a contribution is sent to the server it accepts the contribution and then 
            returns a boolean value indicating if it is acceptable. 
        """
        self.logger.info("@@@@Inside the MyAggregator accept")
        self.logger.info("@@@@ fl_ctx in MyAggregator ")
        current_round = fl_ctx.get_prop(FLConstants.CURRENT_ROUND)
        print("@@@@ current_round ",current_round)
        shared_fl_context = fl_ctx.get_prop(FLConstants.PEER_CONTEXT)
        client_name = shared_fl_context.get_prop(FLConstants.CLIENT_NAME)
        print("@@@@ client_name ", client_name)
        contribution_round = shared_fl_context.get_prop(FLConstants.CURRENT_ROUND)
        print("@@@@ contribution_round ", contribution_round)
        
        #Need to strenght this code to take into consideration all the contribution acceptance scenarios
        if contribution_round == current_round:
            if not self._client_in_accumulator(client_name):
                self.accumulator.append(shared_fl_context)
                self.client_gradient_map[client_name] = shared_fl_context.get_prop(FLConstants.SHAREABLE).get(ShareableKey.META, {}).get('gradients')
                accepted = True
            else:
                self.logger.info('Discarded: Current round: {} contributions already include client: {}'.format(current_round, client_name))
                accepted = False
        else:
            self.logger.info('Discarded the contribution from {} for round: {}. Current round is: {}'.format(client_name, contribution_round, current_round))
            accepted = False
        return (accepted, False)

    def _client_in_accumulator(self, client_name):
        for item in self.accumulator:
            if client_name == item.get_prop(FLConstants.CLIENT_NAME):
                return True
            return False

    def aggregate(self, fl_ctx: FLContext) -> Shareable:
        """This is the main method on the server participating in the federated loop. 
        Inline comments are provided
        """
        self.logger.info("@@@@ In AccumulateWeightedAggregator aggregate ")
        current_round = fl_ctx.get_prop(FLConstants.CURRENT_ROUND)
        print('@@@@ aggregating %s updates at round %s', len(self.accumulator), current_round)

        # All the sharable objects are accumulated in the accumulated method. We extract the weights and graidents. 
        # for Fed_SGD only the gradients are utlized. 
        acc_weights = [set(acc.get_prop(FLConstants.SHAREABLE)[ShareableKey.MODEL_WEIGHTS].keys()) for acc in self.accumulator]
        acc_gradients = [set(acc.get_prop(FLConstants.SHAREABLE)[ShareableKey.META].keys()) for acc in self.accumulator]

        #Combining all teh keys from all the weights and gradients
        acc_weights = (set.union)(*acc_weights)
        acc_gradients = (set.union)(*acc_gradients)

        #Converting the keys into a list
        vars_to_aggregate = [g_var for g_var in acc_weights]
        grads_to_aggreate = [g_grad for g_grad in acc_gradients]

        # Getting the model from the FL_CONTEXT
        m = fl_ctx.get_prop(FLConstants.MODEL_NETWORK)

        flutils.aggregate_gradients(self.model,self.client_gradient_map)
        self.opt.step()
        
        # Creating the order_dict from the model's state_dict with the 
        # updated weights. This is passed on to all the clients. 
        local_dict = m.state_dict()
        local_m_dict = {}
        for var_name in local_dict:
            local_m_dict[var_name] = local_dict[var_name].cpu().numpy()
        else:
            # Creating the sharable object to pass it on to the clients. 
            self.accumulator.clear()
            shareable = Shareable()
            shareable[ShareableKey.TYPE] = ShareableValue.TYPE_WEIGHTS
            shareable[ShareableKey.DATA_TYPE] = ShareableValue.DATA_TYPE_UNENCRYPTED
            shareable[ShareableKey.MODEL_WEIGHTS] = local_m_dict

            fl_ctx.set_prop(FLConstants.SHAREABLE, shareable)
            fl_ctx.set_prop(FLConstants.MODEL_NETWORK, m)

            return shareable