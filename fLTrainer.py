import importlib
from nvflare.apis.event_type import EventType
from nvflare.apis.fl_constant import FLConstants, ShareableKey, ShareableValue
from nvflare.apis.fl_context import FLContext
from nvflare.apis.shareable import Shareable
from nvflare.apis.trainer import Trainer
from nvflare.utils.fed_utils import generate_failure
import torch
from config import get_objects
from config import validate
from mltools import torchutils
import flutils
import mlflow
import random
import time

cuda = torch.device('cuda')
cpu = torch.device('cpu')

class FLTrainer(Trainer):
    """ This class extends the NVFlare Trainer class. This class is reponsible to train the client side of the loop.
    This class needs to extend the train method. 
    """

    def __init__(self):
        """This init method is controlled by the config_fed_client. 
        For FED_SGD we will use one epoch and one iteration in each of the round
        """
        #TODO I need to move all the constants into the config files.
        super().__init__()

    def handle_event(self, event_type:str, fl_context:FLContext):
        """NVFlare uses even driven program. At the start of the federated training
        the START_RUN event is fired. At this point the training dataset, model and loss function are initialized. 
        These objects are retained through out the program. The model which is initialized is just a placeholder. 
        When the training is started, in each round the weights are actually populated from the server.
        """
        if event_type == EventType.START_RUN:
            self.setup(fl_context)
            self.experiment_id = mlflow.create_experiment(f"First Experiment{random.randint(100,9000)}")
            self.experiment = mlflow.get_experiment(self.experiment_id)
            mlflow.start_run()
            self.run = mlflow.active_run()
            print("run_id: {}; status: {}".format(self.run.info.run_id, self.run.info.status))

        if event_type == EventType.END_RUN:
            mlflow.end_run()

    def setup(self, fl_context:FLContext):
        """This method is called in the above handle_event. It holds all the initialzation code for the client side.
        """
        self.client_object = get_objects()
        self.model = self.client_object['model'].to(cuda)
        #validate(self.model, self.client_object['local_trainer'], [self.client_object['dataloader_val']], 0)
        self.it = enumerate(self.client_object['dataloader_train'])


    def train(self, shareable:Shareable, fl_ctx:FLConstants, abort_signal) -> Shareable:
        """This method provides the training on the client side. This is the most important method on the client side.
        Inline comments are provided. Primarily there are three activities
        1. Take the weights from the server stored in the sharable object and assign them to the local learnable object (state_dict)
        2. Perform local training
        3. Convert the learnable to sharable(ordered_dict) to be sent back to the server.
        """
        self.logger.info(f"Trainer abort signal: {abort_signal.triggered}")
        if abort_signal.triggered:
            self.finalize(fl_ctx)
            shareable = generate_failure(fl_ctx=fl_ctx, reason="abort signal triggered")
            return shareable

        # retrieve model weights download from server's shareable. It returns a ordered_dict. This is placed in the sharable
        # on the server using the code present in the aggregator's aggregate method.
        model_weights = shareable[ShareableKey.MODEL_WEIGHTS]
        print(self.model.device)
        self.model.to(cuda)
        print(self.model.device)
        #Prepare the model by updating it with the weights from the server. 
        flutils.replace_model_weights_from_ordered_dict(self.model,model_weights)
        rnd = fl_ctx.get_prop(FLConstants.CURRENT_ROUND)

        #get the batch for the round. 
        try:
            # Getting the batch using the iterator
            batch_idx, batch = next(self.it)
        except StopIteration:
            self.it = enumerate(self.client_object['dataloader_train'])
            batch_idx, batch = next(self.it)

        start = time.time()
        print("@@@@ before global step")
        self.client_object['local_trainer'].fit_loop.global_step = rnd
        print("@@@@ after global step", time.time() - start)

        loss = flutils.do_site_iteration(self.model,batch, batch_idx)

        print("@@@@ after site iteration step", time.time() - start)
        mlflow.log_metric(f"loss", float("{:.4f}".format(loss)))

        validation_rate = len(self.client_object['dataloader_train'])//2
        print("@@@@ after site validation_rate step", time.time() - start)

        # print("@@@ Starting validating...")

        # validate(self.model, self.client_object['local_trainer'], [self.client_object['dataloader_val']], 0)

        if rnd % validation_rate == 0:
            current_epoch = rnd//len(self.client_object['dataloader_train'])
            validate(self.model, self.client_object['local_trainer'], [self.client_object['dataloader_val']], current_epoch)
            print("@@@@ Current round ", rnd)
            print ('Round {}, Loss: {:.4f}'
                .format(rnd, loss.item()))

        

        local_model_dict, local_graident_dict = flutils.get_weights_gradients_order_dict(self.model,cpu)

        # Structure of the Sharable. Model Weights and Gradients are stored into the Sharable.
        # Weights are stored in sharable but are not used in the aggregator. 
        meta_data = {}
        shareable = Shareable()
        meta_data["gradients"] = local_graident_dict
        shareable[ShareableKey.TYPE] = ShareableValue.TYPE_WEIGHTS
        shareable[ShareableKey.DATA_TYPE] = ShareableValue.DATA_TYPE_UNENCRYPTED
        shareable[ShareableKey.MODEL_WEIGHTS] = local_model_dict
        shareable[ShareableKey.META] = meta_data


        self.logger.debug(f"Sending shareable weights to server: \n {shareable[ShareableKey.MODEL_WEIGHTS]}")
        self.logger.debug(f"Sending shareable gradients to server: \n {shareable[ShareableKey.META]}")
        return shareable
