from nvflare.apis.fl_constant import ShareableKey, ShareableValue
from nvflare.apis.fl_context import FLContext
from nvflare.apis.learnable import Learnable
from nvflare.apis.shareable import Shareable
from nvflare.apis.shareable_generator import ShareableGenerator

class FLShareableGenerator(ShareableGenerator):

    def learnable_to_shareable(self, model: Learnable, fl_ctx: FLContext) -> Shareable:
        shareable = Shareable()
        weights_dict = {}
        print("@@@@ MyFullModelShareableGenerator learnable_to_shareable")
        #print("@@@@ Model details \n", [i[0] for i in model.items()])
        #print("@@@@ FL Context Properties")
        #print(fl_ctx.get_all_public_props())
        for key, value in model.items():
            weights_dict.update({key: value})
        else:
            shareable[ShareableKey.TYPE] = ShareableValue.TYPE_WEIGHTS
            shareable[ShareableKey.DATA_TYPE] = ShareableValue.DATA_TYPE_UNENCRYPTED
            #print("@@@@ weight_dict in Sharable Generator learnable to sharable ",weights_dict )
            shareable[ShareableKey.MODEL_WEIGHTS] = weights_dict
            return shareable

    def shareable_to_learnable(self, shareable: Shareable, fl_ctx: FLContext) -> Learnable:
        print("@@@@ MyFullModelShareableGenerator shareable_to_learnable")
        type = shareable[ShareableKey.TYPE]
        model = None
        #print("@@@@ Sharable object \n",shareable.keys())
        if type == ShareableValue.TYPE_WEIGHT_DIFF:
            model = fl_ctx.get_model()
            if shareable[ShareableKey.DATA_TYPE] == ShareableValue.DATA_TYPE_UNENCRYPTED:
                model_diff = shareable[ShareableKey.MODEL_WEIGHTS]
                for v_name, v_value in model_diff.items():
                    v_value += model[v_name]
                    model[v_name] = v_value
        else:
            if type == ShareableValue.TYPE_WEIGHTS:
                model = shareable[ShareableKey.MODEL_WEIGHTS]
                #print("Model after sharable to learnable ", model)
        return model